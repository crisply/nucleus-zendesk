require "faraday"
require "faraday_middleware"
require "hashie"

require "nucleus/zendesk/errors"
require "nucleus/zendesk/version"

module Nucleus
  module Zendesk
  end
end

# Models
require "nucleus/zendesk/models/base"
require "nucleus/zendesk/models/group"
require "nucleus/zendesk/models/metric_set"
require "nucleus/zendesk/models/organization"
require "nucleus/zendesk/models/organization_field"
require "nucleus/zendesk/models/role"
require "nucleus/zendesk/models/ticket"
require "nucleus/zendesk/models/ticket_audit"
require "nucleus/zendesk/models/ticket_event"
require "nucleus/zendesk/models/ticket_field"
require "nucleus/zendesk/models/user"
require "nucleus/zendesk/models/user_field"

require "nucleus/zendesk/client"
require "nucleus/zendesk/connection"
require "nucleus/zendesk/middleware/request/retry"
require "nucleus/zendesk/middleware/response/error_handler"
require "nucleus/zendesk/collection"
require "nucleus/zendesk/incremental_export"
require "nucleus/zendesk/resources"
