module Nucleus
  module Zendesk
    class Error < ::StandardError; end
    class APIError < Error; end
    class ArgumentError < Error; end

    module HTTPError
      class ClientError < ::Faraday::Error::ClientError; end
      class ResourceNotFound < ClientError; end
      class RecordInvalid < ClientError; end
      class TooManyRequests < ClientError; end
    end
  end
end
