module Nucleus
  module Zendesk
    module Middleware
      module Response
        class ErrorHandler < ::Faraday::Response::Middleware

          def on_complete(env)
            case env[:status]
            when 404
              raise Nucleus::Zendesk::HTTPError::ResourceNotFound, response_values(env)
            when 422, 413
              raise Nucleus::Zendesk::HTTPError::RecordInvalid, response_values(env)
            when 100..199, 400..599, 300..303, 305..399
              raise Nucleus::Zendesk::HTTPError::ClientError, response_values(env)
            end
          end

          def response_values(env)
            {url: env.url, status: env.status, headers: env.response_headers, body: env.body}
          end

        end
      end
    end
  end
end

Faraday::Response.register_middleware nucleus_zendesk_error_handler: Nucleus::Zendesk::Middleware::Response::ErrorHandler
