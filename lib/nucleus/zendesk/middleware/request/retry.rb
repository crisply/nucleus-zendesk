module Nucleus
  module Zendesk
    module Middleware
      module Request
        class Retry < Faraday::Middleware

          DEFAULT_RETRY_AFTER = 10
          MAX_WAIT_SECONDS    = 120
          RETRY_CODES         = [429, 503]
          MAX_RETRIES         = 3
          ENV_TO_CLEAR        = %i{status response response_headers}

          attr_reader :logger

          def initialize(app, options={})
            super(app)
            @logger = options[:logger]
            @max_wait_seconds = options[:max_wait_seconds].nil? ? nil : options[:max_wait_seconds].to_i
          end

          def call(env)
            perform_with_retry(env, MAX_RETRIES)
          end


          private


          def perform_with_retry(env, retries_left)
            if retries_left == 0
              raise(Nucleus::Zendesk::HTTPError::TooManyRequests, "Too many requests to Zendesk API.")
            end
            response = @app.call(env)
            response.on_complete do |resp_env|
              if must_retry?(resp_env[:status])
                wait = wait_seconds(resp_env)
                log "Zendesk API: Waiting(#{wait}) for retry because of status(#{resp_env[:status]}) body(#{resp_env[:body]})."
                if wait > MAX_WAIT_SECONDS
                  raise(Nucleus::Zendesk::HTTPError::TooManyRequests, "Too many requests to Zendesk API. status(#{resp_env[:status]}), body(#{resp_env[:body]})")
                end
                sleep wait
                perform_with_retry(update_env(env), retries_left - 1)
              end
            end
            response
          end

          def log(message, severity: :warn)
            return if logger.nil?
            logger.send(severity, message)
          end

          def must_retry?(status)
            RETRY_CODES.include?(status.to_i)
          end

          def update_env(env)
            ENV_TO_CLEAR.each {|key| env.delete key }
            env
          end

          def wait_seconds(resp_env)
            (resp_env[:response_headers][:retry_after] || DEFAULT_RETRY_AFTER).to_i
          end

          def max_wait_seconds
            @max_wait_seconds || MAX_WAIT_SECONDS
          end

        end
      end
    end
  end
end

Faraday::Middleware.register_middleware nucleus_zendesk_retry: Nucleus::Zendesk::Middleware::Request::Retry
