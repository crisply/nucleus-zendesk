module Nucleus
  module Zendesk
    class Resources
      SUPPORTED_TYPES = %w{organization ticket user}

      def self.res_get(name, collection_type: nil, required_params: [], required_exclusive_params: [], optional_params: [], collection_class: Collection)
        method_name = name.to_s.gsub("/", "_")

        define_method(method_name) do |*args|
          params = Hashie::Mash.new(args[0])
          process_params!(params, required_params: required_params,
                         required_exclusive_params: required_exclusive_params,
                         optional_params: optional_params)
          resp = client.api_get(name, params).body
          includes = get_includes(resp, params)
          if params.include?("id")
            key = resp.keys.first
            model_for(key).new(resp[key], includes: includes)
          else
            collection_class.new client, resp, includes: includes
          end
        end

        unless Nucleus::Zendesk::Client.instance_methods.include? method_name.to_sym
          Nucleus::Zendesk::Client.def_delegator :@resources, method_name
        end
      end

      res_get :organization_fields
      res_get :organizations, optional_params: %w{id}
      res_get "organizations/show_many", required_exclusive_params: %w{ids external_ids}
      res_get "search", required_params: %w{query}
      res_get :ticket_fields
      res_get :tickets, optional_params: %w{id}
      res_get "tickets/show_many", required_params: %w{ids}
      res_get "ticket/audits", required_params: %w{ticket_id}, optional_params: %w{include}
      res_get :user_fields
      res_get :users, optional_params: %w{id}
      res_get "users/show_many", required_exclusive_params: %w{ids external_ids}

      res_get "incremental/organizations",
        required_params: %w{start_time}, optional_params: %w{include},
        collection_class: IncrementalExport
      res_get "incremental/tickets",
        required_params: %w{start_time}, optional_params: %w{include},
        collection_class: IncrementalExport
      res_get "incremental/ticket_events",
        required_params: %w{start_time},
        collection_class: IncrementalExport

      attr_reader :client

      def initialize(client)
        @client = client
      end

      def model_for(key)
        if SUPPORTED_TYPES.include?(key)
          Nucleus::Zendesk.const_get(key.capitalize)
        else
          raise(Nucleus::Zendesk::Error, "Not a supported resource: #{key}")
        end
      end


      private


      def get_includes(response, params)
        includes = Hashie::Mash.new
        return includes unless params.include?("include")
        types = params["include"].to_s.strip.split(/\s*,\s*/)
        types.each_with_object(includes) {|inc,all|
          next unless response[inc]
          includes[inc] = Collection.new(client, Hashie::Mash[{inc => response[inc]}])
        }
      end

      def process_params!(params, required_params:, required_exclusive_params:, optional_params:)
        required_params.each {|p|
          next if params.include?(p)
          raise Nucleus::Zendesk::APIError, "Required parameter missing: #{p}"
        }
        unless required_exclusive_params.empty?
          matches = required_exclusive_params.each_with_object([]) {|p,matches|
            matches << p if params.include?(p)
          }
          if matches.length != 1
            raise Nucleus::Zendesk::APIError, "Incorrect parameters: #{params}"
          end
        end
        params.each_pair {|k,v|
          case v
          when Array
            params[k] = v.join(',')
          when DateTime,Time
            if %w{start_time end_time}.include? k
              params[k] = v.to_time.utc.to_i
            else
              params[k] = v.to_time.utc.iso8601
            end
          end
        }
      end

    end
  end
end
