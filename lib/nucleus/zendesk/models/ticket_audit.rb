module Nucleus
  module Zendesk
    class TicketAudit < Base

      def author
        return nil if author_id.nil?

        if includes.include? "users"
          includes.users.select{|o| o.id == author_id}.first
        else
          # @todo Add some client fetching capabilities
          raise ArgumentError, "Could not retrieve organization"
        end
      end

    end
  end
end
