module Nucleus
  module Zendesk
    class Ticket < Base

      def organization
        return @organization if defined? @organization
        @organiztion = begin
          if organization_id.nil?
            nil
          elsif includes.include? "organizations"
            includes.organizations.select{|o| o.id == organization_id}.first
          else
            raise ArgumentError, "Could not retrieve organization"
          end
        end
      end

    end
  end
end
