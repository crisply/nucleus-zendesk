module Nucleus
  module Zendesk
    class Base < SimpleDelegator

      attr_accessor :includes

      def initialize(obj, includes: nil)
        super(obj)
        @includes = includes || Hashie::Mash.new
      end

    end
  end
end
