module Nucleus
  module Zendesk
    class User < Base

      def organization
        return nil if organization_id.nil?

        if includes.include? "organizations"
          includes.organizations.select{|o| o.id == organization_id}.first
        else
          # @todo Add some client fetching capabilities
          raise ArgumentError, "Could not retrieve organization"
        end
      end

    end
  end
end
