module Nucleus
  module Zendesk
    class Connection
      extend Forwardable

      def_delegators :@http, :get

      attr_reader :http, :logger

      def initialize(url:, **opts)
        if opts[:oauth_token]
          auth_init = [:authorization, :Bearer, opts[:oauth_token]]
        elsif opts[:email] && opts[:api_token]
          auth_init = [:basic_auth, "#{opts[:email]}/token", opts[:api_token]]
        elsif opts[:username] && opts[:password]
          auth_init = [:basic_auth, opts[:username], opts[:password]]
        else
          raise ::ArgumentError, "missing authentication credentials"
        end
        @max_retry_wait = opts[:max_retry_wait]
        @logger = opts[:logger]
        @http = initialize_connection(url) {|c|
          c.send *auth_init
        }
      end


      private


      def default_adapter
        Faraday.default_adapter
      end

      def initialize_connection(url)
        Faraday.new(url: url) do |c|
          yield c if block_given?

          # -- response --
          c.response :nucleus_zendesk_error_handler
          c.response :mashify
          c.response :json, content_type: /\bjson/

          # -- request --
          c.request :multipart
          c.request :json
          c.use :nucleus_zendesk_retry, logger: logger, max_wait_seconds: @max_retry_wait

          c.adapter default_adapter
        end
      end

    end
  end
end
