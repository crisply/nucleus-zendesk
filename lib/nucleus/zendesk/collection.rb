module Nucleus
  module Zendesk
    class Collection
      extend Forwardable
      include Enumerable

      TYPE_MAP = {
        "audit"  => Nucleus::Zendesk::TicketAudit,
        "audits"  => Nucleus::Zendesk::TicketAudit,
        "group"  => Nucleus::Zendesk::Group,
        "groups"  => Nucleus::Zendesk::Group,
        "metric_set" => Nucleus::Zendesk::MetricSet,
        "metric_sets" => Nucleus::Zendesk::MetricSet,
        "organization" => Nucleus::Zendesk::Organization,
        "organization_field" => Nucleus::Zendesk::OrganizationField,
        "organization_fields" => Nucleus::Zendesk::OrganizationField,
        "organizations" => Nucleus::Zendesk::Organization,
        "role" => Nucleus::Zendesk::Role,
        "roles" => Nucleus::Zendesk::Role,
        "ticket" => Nucleus::Zendesk::Ticket,
        "ticket_events" => Nucleus::Zendesk::TicketEvent,
        "ticket_field" => Nucleus::Zendesk::TicketField,
        "ticket_fields" => Nucleus::Zendesk::TicketField,
        "tickets" => Nucleus::Zendesk::Ticket,
        "user"   => Nucleus::Zendesk::User,
        "user_field"   => Nucleus::Zendesk::UserField,
        "user_fields"   => Nucleus::Zendesk::UserField,
        "users"   => Nucleus::Zendesk::User,
      }.freeze

      SUPPORTED_TYPES = %w{audits groups metric_sets organization_fields
        organizations roles ticket_events ticket_fields tickets user_fields
        users results}.freeze

      attr_reader :client, :type, :response

      def_delegators :@response, :next_page, :previous_page, :count

      def initialize(client, response, includes: nil)
        @client = client
        @includes = includes || Hashie::Mash.new
        @type = (SUPPORTED_TYPES & (response.keys - @includes.keys)).first
        raise(Nucleus::Zendesk::Error, "No type for collection") if @type.nil?
        @response = response
      end

      def each_page
        return to_enum(__method__) unless block_given?
        begin
          yield response.send(type).each_with_object([]) {|o, page|
            page << TYPE_MAP[o.result_type || type].new(o, includes: @includes)
          }
        end while get_next_page!
      end

      def each(&block)
        return to_enum(__method__) unless block_given?
        each_page do |page|
          page.each{|o| yield o }
        end
      end

      def count
        response["count"]
      end

      def includes
        @includes ||= (response.keys - [type, "next_page", "previous_page", "count"])
      end

      def includes?(inc)
        includes.include?(inc)
      end


      private


      def get_next_page!
        return false if next_page.nil?
        @response = client.connection.get(next_page).body
      end

    end
  end
end
