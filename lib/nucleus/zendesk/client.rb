module Nucleus
  module Zendesk
    class Client
      extend Forwardable

      API_BASE = "/api/v2"

      RESOURCE_MAP = {
        "ticket" => "tickets",
        "user"   => "users",
      }

      attr_reader :connection, :resources

      def initialize(**opts)
        @connection = Connection.new(**opts)
        @resources  = Resources.new(self)
      end

      def api_get(path, params)
        parts  = path.to_s.split("/")
        params = Hashie::Mash.new(params)
        fullpath = parts.each_with_object([api_base]) do |p, fp|
          fp << resource_lookup(p)
          if params.include?("id")
            fp << params.delete("id")
          elsif params.include?("#{p}_id")
            fp << params.delete("#{p}_id")
          end
        end
        #@todo add some logging here
        #sparams = params.collect{|k,v| "#{k}=#{v}"}.join('&')
        #puts "-----------------------------------------------------------------------"
        #puts "---- REQUEST: #{fullpath.join('/')}.json?#{sparams}"
        #puts "-----------------------------------------------------------------------"
        connection.get "#{fullpath.join('/')}.json", params
      end

      def organization_fields
        @organization_fields ||= @resources.organization_fields
      end

      def ticket_fields
        @ticket_fields ||= @resources.ticket_fields
      end

      def uri
        connection.http.url_prefix
      end


      private


      def api_base
        API_BASE
      end

      def resource_lookup(name)
        RESOURCE_MAP[name.to_s] || name
      end

    end
  end
end
