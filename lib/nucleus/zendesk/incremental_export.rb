module Nucleus
  module Zendesk
    class IncrementalExport < Collection

      ONE_HOUR   = 3600
      EXPORT_MAX = 1000

      def_delegator :@response, :end_time

      def includes
        @includes ||= (super - ["end_time"])
      end


      private


      def get_next_page!
        return false if count < EXPORT_MAX
        super
      end

    end
  end
end
