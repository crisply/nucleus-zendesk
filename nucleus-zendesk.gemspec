# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'nucleus/zendesk/version'

Gem::Specification.new do |spec|
  spec.name          = "nucleus-zendesk"
  spec.version       = Nucleus::Zendesk::VERSION
  spec.authors       = ["Dan Wanek"]
  spec.email         = ["dan@crisply.com"]

  spec.summary       = %q{A light-weight client for the Zendesk V2 REST API.}
  spec.description   = %q{A light-weight client for the Zendesk V2 REST API.}
  spec.homepage      = "https://github.com/crisply/nucleus-zendesk.git"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "faraday", "~> 0.9.0"
  spec.add_dependency "faraday_middleware", ">= 0.9.0", "< 0.11.0"
  spec.add_dependency "hashie", "~> 3.4.0"

  spec.add_development_dependency "bundler", "~> 1.9"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "pry"
  spec.add_development_dependency "mocha", "~> 1.1.0"
end
