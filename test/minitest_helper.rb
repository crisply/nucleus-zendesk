$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'nucleus/zendesk'
require 'minitest/autorun'
require 'mocha/mini_test'
require 'pry'
require 'json'

TEST_DIR = File.dirname(__FILE__)

module NucleusTestHelpers
  def fixture_file_path(name)
    "#{TEST_DIR}/fixtures/#{name}"
  end

  def load_sample(name)
    File.read fixture_file_path(name)
  end

  def load_json(name)
    JSON.parse load_sample(name)
  end
end

class MiniTest::Spec
  include NucleusTestHelpers
end
