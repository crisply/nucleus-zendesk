require 'minitest_helper'

module Nucleus::Zendesk
  describe Client do
    let(:inst) { Client.new }

    before do
      Connection.stubs(:new)
    end

    describe "resource method" do
      it { inst.must_respond_to(:organizations) }
      it { inst.must_respond_to(:tickets) }
      it { inst.must_respond_to(:ticket_audits) }
      it { inst.must_respond_to(:users) }
    end

  end
end
