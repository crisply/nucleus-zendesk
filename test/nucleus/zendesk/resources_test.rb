require "minitest_helper"

module Nucleus::Zendesk
  describe Resources do

    describe "#model_for" do
      let(:inst) { Resources.new(nil) }
      it { inst.model_for("organization").must_equal Nucleus::Zendesk::Organization }
      it { inst.model_for("ticket").must_equal Nucleus::Zendesk::Ticket }
      it { inst.model_for("user").must_equal Nucleus::Zendesk::User }
    end

    describe "#organizations" do
      let(:client) { mock("Client") }
      let(:inst) { Resources.new(client) }
      let(:orgs_json) { load_json("organizations.json") }

      it "returns a collection of Organizations" do
        client.expects(:api_get).with(:organizations, {}).returns(Hashie::Mash[body: orgs_json])
        resp = inst.organizations
        resp.must_be_instance_of Collection
        resp.each.must_be_instance_of Enumerator
        resp.count.must_equal 3
        resp.each.collect(&:name).must_equal(["Crisply, Inc.", "Daniel LLC", "Abshire and Sons"])
      end
    end

    describe "#search" do
      let(:client) { mock("Client") }
      let(:inst) { Resources.new(client) }
      let(:user_search_json) { load_json("search-users.json") }

      it "returns results of a user query" do
        client.expects(:api_get).with("search", {"query" => "type:user email:crisplydev@crisply.com email:lynch.wallace@ruel.net"}).returns(Hashie::Mash[body: user_search_json])
        resp = inst.search(query: "type:user email:crisplydev@crisply.com email:lynch.wallace@ruel.net")
        resp.must_be_instance_of Collection
        resp.each.must_be_instance_of Enumerator
        resp.count.must_equal 2
        resp.each {|i| i.class.must_equal User }
      end
    end

    describe "#tickets" do
      let(:client) { mock("Client") }
      let(:inst) { Resources.new(client) }
      let(:tickets_json) { load_json("tickets.json") }

      it "returns a collection of Organizations" do
        client.expects(:api_get).with(:tickets, {"include" => :users}).returns(Hashie::Mash[body: tickets_json])
        resp = inst.tickets(include: :users)
        resp.must_be_instance_of Collection
        resp.each.must_be_instance_of Enumerator
        resp.count.must_equal 5
        resp.each.collect(&:subject).must_equal( ["If we quantify t", "If we reboot the", "bypassing the mo", "You can't index ", "synthesizing the"] )
      end
    end

    describe "#users" do
      let(:client) { mock("Client") }
      let(:inst) { Resources.new(client) }
      let(:users_json) { load_json("users.json") }
      let(:orgs_json) { load_json("organizations.json") }

      it "returns a collection of Users" do
        client.expects(:api_get).with(:users, {}).returns(Hashie::Mash[body: users_json])
        resp = inst.users
        resp.must_be_instance_of Collection
        resp.each.must_be_instance_of Enumerator
        resp.count.must_equal 5
        resp.each.collect(&:email).must_equal(
          ["crisplydev@crisply.com", "zendesk-api-client-ruby-agent-dan@crisply.com",
           "pfannerstill_angel@kuhnschulist.net", "zendesk-api-client-ruby-anonymous-dan@crisply.com",
           "lynch.wallace@ruel.net"
          ])
      end

      it "returns a collection of Users with side-loaded organizations" do
        users_json.merge!({"organizations" => orgs_json["organizations"]})
        client.expects(:api_get).with(:users, {"include" => "organizations"}).returns(Hashie::Mash[body: users_json])
        resp = inst.users("include" => "organizations")
        resp.must_be_instance_of Collection
        resp.each.must_be_instance_of Enumerator
        resp.count.must_equal 5
        resp.to_a[0].organization.name.must_equal "Crisply, Inc."
      end
    end

  end
end
